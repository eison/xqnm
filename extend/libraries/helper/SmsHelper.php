<?php
// +----------------------------------------------------------------------+
// | 发送短信助手类，支持继承重写
// +----------------------------------------------------------------------+
namespace libraries\helper;

class SmsHelper
{
    /**
     * 配置参数
     * @var array
     */
    protected static $options = [];


    /**
     * 发起发送短信请求
     *
     * @param string $params 请求参数
     * @param bool $ispost 请求方式（true：post、false：get）
     * @return mixed
     */
    protected static function juheCurl($params = false, $ispost = false)
    {
        $ch = curl_init();
        $url = 'http://v.juhe.cn/sms/send';

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'JuheData');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        if ($ispost) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_URL, $url);
        } else {
            if ($params) {
                curl_setopt($ch, CURLOPT_URL, $url . '?' . $params);
            } else {
                curl_setopt($ch, CURLOPT_URL, $url);
            }
        }

        if (FALSE === ($response = curl_exec($ch))) {
            return false;
        }

        curl_close($ch);
        return $response;
    }

    /**
     * 缓存验证码
     *
     * @param $phone 发送短信的手机号码
     * @param $code  验证码
     * @param $time  缓存时间（单位秒）
     * @return void
     */
    protected static function cache($phone, $code, $time)
    {
        CacheHelper::set("sms-send::{$phone}", $code, $time);
    }

    /**
     * 初始化设置参数
     *
     * @param $options 自定义配置参数
     * @return void
     */
    public static function init(array $options = [])
    {
        $_options = [
            'key'    => '2a7319cd10e8585ee91cb1e72daef217',  // 您申请的APPKEY
            'tpl_id' => '122967',                            // 您申请的短信模板ID，根据实际情况修改
        ];

        static::$options = array_merge($_options, $options);
        return static::class;
    }

    /**
     * 手机号合法判断
     *
     * @param $phone 发送短信的手机号码
     * @return bool
     */
    public static function isPhone($phone)
    {
        $string = '/^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/';
        return preg_match($string, $phone) ? true : false;
    }

    /**
     * 判断短信验证码是否到期
     *
     * @param $phone 发送短信的手机号码
     * @return bool
     */
    public static function expire($phone)
    {
        $result = CacheHelper::get("sms-send::{$phone}", false);
        return $result ? false : true;
    }

    /**
     * 删除短信验证码
     *
     * @param $phone
     * @return void
     */
    public static function delete($phone)
    {
        CacheHelper::delete("sms-send::{$phone}");
    }

    /**
     * 发送短信接口
     *
     * @param $phone  发送短信的手机号码
     * @param $caceh  是否缓存验证码
     * @param $code   验证码（缺省为6位纯数字）
     * @param $time   验证码有效期 （单位秒）
     * @return mixed
     * @throws \think\Exception
     */
    public static function send($phone, $caceh = true, $code = null, $time = 90)
    {
        $code = $code ? strval($code) : verification_random(6, true);

        $params['mobile'] = trim($phone);
        $params['tpl_value'] = "#code#={$code}&#company#=青柠檬"; // 模板变量，根据实际情况修改

        if (!static::$options) {
            throw new \think\Exception('invalid paramters');
        }

        $params = http_build_query(array_merge(static::$options, $params));
        $result = json_decode(static::juheCurl($params), true);

        if ($result && $caceh) {
            static::cache($phone, $code, $time);
        }

        return $result;
    }
}
