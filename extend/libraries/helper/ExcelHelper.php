<?php
// +----------------------------------------------------------------------+
// | Excel操作助手类
// +----------------------------------------------------------------------+
namespace libraries\helper;

use think\Exception;
use think\Loader;
 
class ExcelHelper
{
    /**
     * 单例对象
     * @var null
     */
    protected static $objPHPExcel = NULL;


    /**
     * 保存验证器对象
     * @var null
     */
    protected static $validate = NULL;


    /**
     * 宝贝连接id
     * @var null
     */
    protected static $badyLinkId = 0;


    /**
     * 验证文件后缀名
     *
     * @param $fullFilePath 完整的文件路径名称
     * @return void
     */
    private static function checkExt($fullFilePath): void
    {
        // 此处不重复验证后缀名，请在接收文件时就进行验证
        if (!is_file($fullFilePath)) {
            throw new Exception('file not exist!!');
        }
    }

    /**
     * 获取验证器单例对象
     *
     * @param void
     * @return object  返回公用验证器对象
     */
    private static function getValidate(): object
    {
        if (!is_object(static::$validate)) {
            static::$validate = Loader::validate('common/ImportTask');
        }

        return static::$validate;
    }

    /**
     * 初始化 PHPExecl
     *
     * @param $fullFilePath  完整的文件路径名称
     * @throws \PHPExcel_Reader_Exception
     */
    private static function init($fullFilePath): void
    {
        if (is_null(static::$objPHPExcel)) {
            vendor('PHPExcel.IOFactory');
            $fileInfo = explode('.', $fullFilePath);

            if (end($fileInfo) == 'xlsx') {
                $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
            } else if (end($fileInfo) == 'xls') {
                $objReader = \PHPExcel_IOFactory::createReader('Excel5');
            }

            static::$objPHPExcel = $objReader->load($fullFilePath);
        }
    }

    /**
     * 初始化一个 excel 句柄 （支持链式调用）
     *
     * @param $fullFilePath 完整的文件路径名称
     * @return string       返回带完整命名空间的 ExcelHelper 类
     */
    public static function fopen($fullFilePath): string
    {
        static::checkExt($fullFilePath);
        static::init($fullFilePath);

        return ExcelHelper::class;
    }

    /**
     * 获取指定视图下的所有行
     *
     * @param string $column  要获取的列（,拼接的字符串）
     * @param int $number     从第几个sheet中获取数据
     * @return array          返回取到的结果集
     */
    public static function getAllRows(?string $column, int $number = 0): array
    {
        $rows = static::$objPHPExcel->getSheet($number)->getHighestRow();
        $columns = explode(',', $column);
        $results = array();

        foreach (__yield($rows + 1, 2) as $i) {
            foreach ($columns as $field) {
                $value = static::$objPHPExcel->getActiveSheet()->getCell($field . $i)->getValue();
                $results[$i][] = is_object($value) ? $value->__toString() : $value;
            }

            if (!trim(implode('', array_values($results[$i])))) {
                unset($results[$i]);
                break;  // 每行间不能存在空行
            }
        }

        return $results;
    }

    /**
     * 将 Excel 保存的时间转为格林威治时间
     *
     * @param mixed $time  excel 中保存的任务时间
     * @return string      返回格林威治时间 2019-06-20 格式
     */
    public static function specifTaskDate($time = NULL): string
    {
        $time = is_numeric($time)
            ? gmdate('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($time))
            : ($time ?: specif_time());

        return $time;
    }

    /**
     * 将 Excel 保存的商品宝贝连接 url 标准化
     *
     * @param string $url 商品宝贝连接 url
     * @return string
     */
    public static function specifBadyLink(?string $url): string
    {
        preg_match('/id=\d+/', strval($url), $pattern); // 提取url地址中的商品id

        if ($pattern) {
            list($url) = explode('?', $url);
            $url = $url . '?' . current($pattern);

            static::$badyLinkId = (int)explode('=', reset($pattern))[1]; // 优化：避免在循环中 getBadyLinkId() 二次解析url
        }

        return $url;
    }

    /**
     * 获取宝贝连接url的商品id
     *
     * @param string $url 商品宝贝连接 url
     * @return int        返回商品id
     */
    public static function getBadyLinkId(?string $url): int
    {
        if (!static::$badyLinkId) {
            preg_match('/id=\d+/', strval($url), $pattern); // 提取url地址中的商品id
            static::$badyLinkId = (int)explode('=', reset($pattern))[1];
        }

        return static::$badyLinkId;
    }

    /**
     * 将任务时间段分解为多条任务
     *
     * @param array $row  一行数据
     * @return array
     */
    public static function breakTasks(array $row)
    {
        $timeSlot = explode(';', $row['timeSlot']);
        $totalRow = array();

        array_walk($timeSlot, function ($value) use (&$totalRow, &$row) {
            if (trim($value)) {
                list($tasktimepart, $number) = explode('-', $value);

                $row['tasknum'] = $number;
                $row['timenum'] = $value;
                $row['tasktimepart'] = $tasktimepart;
                $totalRow[] = $row;
            }
        });

        return $totalRow;
    }

    /**
     * 导入任务格式验证器
     *
     * @param $datas  待验证数据
     * @param $cond   附加条件
     */
    public static function validate(?array $datas, array $cond): void
    {
        $result = static::getValidate()->check($datas, [], '', $cond);

        if (!$result) {
            header('Content-Type:application/json; charset=utf-8');
            exit(json_encode(['statusCode' => 112007, 'msg' => static::getValidate()->getError()]));
        }
    }

    /**
     * 导出 Excel 文件
     *
     * @param string $expTitle     文件名称
     * @param string $expCellName  cell名称
     * @param string $expTableData 导出数据
     */
    public static function exportExcel($expTitle, $expCellName, $expTableData)
    {
        vendor('PHPExcel');

        $xlsTitle = iconv('utf-8', 'gb2312', $expTitle);  // 文件名称
        $cellNum = count($expCellName);
        $dataNum = count($expTableData);
        $objPHPExcel = new \PHPExcel();
        $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

        $objPHPExcel->getActiveSheet(0)->mergeCells('A1:' . $cellName[$cellNum - 1] . '1');   // 合并单元格
        for ($i = 0; $i < $cellNum; $i++) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i] . '2', $expCellName[$i][1]);
        }

        // 导出数据库数值
        for ($i = 0; $i < $dataNum; $i++) {
            for ($j = 0; $j < $cellNum; $j++) {
                $objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j] . ($i + 3), $expTableData[$i][$expCellName[$j][0]]);
            }
        }

        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="' . $xlsTitle . '.xls"');
        header("Content-Disposition:attachment;filename=$expTitle.xls");

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

        exit;
    }
}
