<?php
// +----------------------------------------------------------------------+
// | 缓存助手类，支持继承重写，缓存对象依赖注入
// +----------------------------------------------------------------------+
namespace libraries\helper;

class CacheHelper
{
    /**
     * 缓存对象（单例对象）
     * @var null
     */
    private static $cache = NULL;


    /**
     * 初始化对象（支持依赖注入）
     *
     * @param object Redis object
     * @return object self
     * @throws Exception
     */
    public static function init($cache = NULL)
    {
        // $cache = new \think\Session();

        if (!is_object(static::$cache)) {
            static::$cache = is_object($cache) ? $cache : new \think\cache\driver\Redis();
        }

        return static::class;
    }

    /**
     * 写入缓存
     *
     * @param string $name Redis 缓存中保存的key
     * @param string $value Redis 缓存中保存的value
     * @param int $expire Redis 有效时间（秒）
     * @return bool
     */
    public static function set($name, $value, $expire = null)
    {
        $result = static::$cache->set($name, $value, $expire);
        return !$result ?: $value;
    }

    /**
     * 获取缓存
     *
     * @param string $name Redis 缓存中保存的key
     * @param mixed $default 未取到缓存时返回自定义值
     * @return mixed
     */
    public static function get($name, $default = false)
    {
        $result = static::$cache->get($name);
        return $result ?: $default;
    }

    /**
     * 判断缓存
     *
     * @param string $name 缓存变量名
     * @return bool
     */
    public static function has($name)
    {
        $key = static::$cache->getCacheKey($name);
        return static::$cache->handler->exists($key);
    }

    /**
     * 删除缓存
     *
     * @param string $name 缓存变量名
     * @return boolean
     */
    public static function delete($name)
    {
        $key = static::$cache->getCacheKey($name);
        return static::$cache->handler->delete($key);
    }

    /**
     * 查找保存键
     *
     * @Description 支持模糊查询，有3个通配符 *, ? ,[]
     *      *: 通配任意多个字符
     *      ?: 通配单个字符
     *      []: 通配括号内的某1个字符
     *
     * @param string $pattern
     * @return mixed
     */
    public static function seachKey($pattern)
    {
        $keys = static::$cache->handler->keys($pattern);
        return $keys ?: false;
    }

    /**
     * 销毁所有缓存数据
     *
     * @param string $tag 标签名
     * @return void
     */
    public static function destroy($tag = null)
    {
        static::$cache->clear($tag);
    }
}
