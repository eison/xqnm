<?php
// +----------------------------------------------------------------------+
// | 金额操作助手类
// +----------------------------------------------------------------------+
namespace libraries\helper;

class MoneyHelper
{
    /**
     * 元转分（以分为单位）
     *
     * @param $money    元转分
     * @return array|int
     */
    public static function toPenny($money)
    {
        if (is_array($money)) {
            foreach ($money as $index => $unit) {
                $money[$index] = static::toPenny($unit);
            }
        } else {
            $money = $money * 100;
        }

        return $money;
    }

    /**
     * 分转元（以元为单位）
     *
     * @param $money    分转元
     * @return array|int
     */
    public static function toDollar($money)
    {
        if (is_array($money)) {
            foreach ($money as $index => $unit) {
                $money[$index] = static::toDollar($unit);
            }
        } else {
            $money = $money / 100;
        }

        return $money;
    }

    /**
     * 格式化金额
     *
     * @param $money    格式化金额
     * @return array|string
     */
    public static function toFormat($money)
    {
        if (is_array($money)) {
            foreach ($money as $index => $unit) {
                $money[$index] = static::toFormat($unit);
            }
        } else {
            $money = number_format($money, 2, '.', ',');
        }

        return $money;
    }
}
