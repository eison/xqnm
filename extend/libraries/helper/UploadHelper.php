<?php
// +----------------------------------------------------------------------+
// | 文件上传助手类，支持继承重写
// +----------------------------------------------------------------------+
namespace libraries\helper;

class UploadHelper
{
    /**
     * 保存文件
     *
     * @param $savePath  保存路径
     * @param string $fileName  保存文件名(不加后缀)
     * @return bool|string  返回完整地图片保存路径
     */
    public static function save($savePath, $fileName = NULL)
    {
        preg_match('/^(data:\s*image\/(\w+);base64,)/', static::$base64, static::$imageInfo);

        $realPath = __UPLOADS__ . $savePath;
        !file_exists($realPath) and mkdir($realPath, 0777);

        $fileName = $fileName ?: md5(create_uuid());
        $suffix = '.' . end(static::$imageInfo);

        $file = base64_decode(str_replace(static::$imageInfo[1], '', static::$base64));
        $fullPath = "{$realPath}/{$fileName}{$suffix}";

        return file_put_contents($fullPath, $file) ? $fullPath : false;
    }
}
