<?php
// +----------------------------------------------------------------------+
// | 数组操作助手类
// +----------------------------------------------------------------------+
namespace libraries\helper;

class ArrayHelper
{
    /**
     * 根据指定的键对数组排序
     *
     * @param array $array 要排序的数组
     * @param string $keyname 排序的键
     * @param int $dir 排序方向   具体参考 array_multisort() 函数 排序顺序标志
     * @return array 排序后的数组
     */
    static public function sort(array $array, string $keyname, $dir = SORT_ASC)
    {
        return static::sort_byMultiCols($array, [$keyname => $dir]);
    }

    /**
     * 将一个二维数组按照多个列进行排序，类似 SQL 语句中的 ORDER BY
     *
     * @param array $rowset 要排序的数组
     * @param array $args 排序的键
     * @return array 排序后的数组
     */
    static public function sort_byMultiCols(array $rowset, array $args)
    {
        $sortRule = '';
        $sortArray = array();

        foreach ($args as $sortField => $sortDir) {
            foreach ($rowset as $offset => $row) {
                $sortArray[$sortField][$offset] = $row[$sortField];
            }

            $sortRule .= '$sortArray[\'' . $sortField . '\'], ' . $sortDir . ', ';
        }

        if (empty($sortArray) || empty($sortRule)) {
            return $rowset;
        }

        eval('array_multisort(' . $sortRule . '$rowset);');
        return $rowset;
    }

    /**
     * 将两个二维数组按照指定列进行合并，类似 SQL 语句中的 left join
     *
     * @param array $array 合并的数组 也是最终要生成的数组
     * @param array $combined 被合并的数组
     * @param string $field   指定相等字段
     * @return array 满足条件后合并的数组
     */
    static public function leftJoin(array $array, array $combined, string $field)
    {
        foreach ($combined as $index => $row) {
            $temp[$row[$field]] = $row;
        }

        foreach ($array as $index => $row) {
            $target[] = array_merge((array)$temp[$row[$field]], $row);
        }

        return $target;
    }

    /**
     * 将一个二维数组按照多列进行分组，类似 SQL 语句中的 group by
     *
     * @param array $array 需要进行分组的数组
     * @param string $keyname 接收一个或多个参数（分组字段）
     * @return array 合并后的新数组
     */
    static public function group(array $array, ...$colArray)
    {
        $groupArray = array();
        $colArray = func_get_args();

        array_shift($colArray);
        foreach (array_reverse ($array) as $index => $arr) {
            $keyname = '';
            foreach ($colArray as $value) {
                $keyname .= $arr[$value] . ':';
            }

            $groupArray[$keyname] = $arr;
        }

        return array_values($groupArray);
    }

    /**
     * 将一个二维数组按指定字段分组，并求和 类似于 sum() 函数 group by column
     *
     * @param array $array 需要计算的数组
     * @param string $groupCol 分组字段
     * @param string $sumCol 求和字段
     * @return array 返回以分组字段值为key求和值为数组值的新数组
     */
    static public function groupSum(array $array, string $groupCol, string $sumCol)
    {
        $sumArray = array();
        foreach ($array as $index => $map) {
            $sumArray[$map[$groupCol]] += $map[$sumCol];
        }

        return $sumArray;
    }
}
