<?php
// +----------------------------------------------------------------------+
// | 基于逻辑控制器与路由控制器的中间层抽象
// +----------------------------------------------------------------------+
// | data：2019-04-29 15:28:35  by eison
// +----------------------------------------------------------------------+
namespace libraries\midware;

use think\Controller;

class MidwareController extends Controller
{
    use Setter;

    /**
     * trim String
     *
     * 全局统一以字符串进行数据处理
     * @return mixed
     */
    private function trimString($mVal, $rules)
    {
        if (is_array($mVal)) {
            foreach ($mVal as $key => $val) {
                $mValue[$key] = $this->trimString($val, $rules);
            }
        } else {
            if (null !== $mVal && strlen($mVal) > 0) {
                foreach ($rules as $func) {
                    $mValue = (string)call_user_func($func, $mVal); // 支持系统内建函数和自定义函数
                }
            } else {
                $mValue = '';
            }
        }

        return $mValue;
    }

    /**
     * 表单数据库字段映射转换
     *
     * @param array $inputs  输入表单数据
     * @param array $mapping 映射数组
     * @return array
     */
    protected function convert(array $inputs, array $mapping = []): array
    {
        if ($inputs) {
            if (!$mapping) {
                list($model, $controller, $action) = explode('/', $this->request->pathinfo());
                $mapping = (array)\config(implode('.', [$model, $controller]))[$action]; // 有可能没数据
            }

            if ($mapping) {
                foreach ($mapping as $field => $alias) {
                    if (isset($inputs[$alias])) {
                        $inputs[$field] = (null !== $inputs[$alias]) ? $inputs[$alias] : '';
                        unset($inputs[$alias]);
                    }
                }
            }
        }

        return (array)$inputs;
    }

    /**
     * 初始化请求状态
     */
    protected function _initialize()
    {
        parent::_initialize();

        $this->setCode(300);
        $this->setStatusCode(110001);
    }

    /**
     * 表单数据安全过滤
     *
     * @param mixed   $mVal 用户输入数据
     * @param string  $rule 滤规则（支持自定义函数） 格式：funcname/funcname/funcname
     * @return string       返回过虑后的最终数据
     */
    protected function inputFilter($mVal = NULL, string $rule = NULL)
    {
        $mVal = $mVal ?: NULL;
        $rule = $rule ?: 'addslashes/htmlentities/strip_tags/trim';
        return $this->trimString($mVal, explode('/', $rule));
    }

    /**
     * 设置返回结果
     *
     * @param array $data
     * @return void
     */
    protected function returnRes(array $data = []): void
    {
        // 返回JSON数据格式到客户端 包含状态信息
        header('Content-Type:application/json; charset=utf-8');
        exit(json_encode(array_merge($this->parameters, (array)$data)));
    }

    /**
     * 404 错误页面
     * @return string
     */
    public function _empty()
    {
        return $this->fetch('public/error');
    }
}
