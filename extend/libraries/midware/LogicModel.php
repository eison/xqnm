<?php
// +----------------------------------------------------------------------+
// | 基础逻辑模型
// +----------------------------------------------------------------------+
namespace libraries\midware;

class LogicModel
{
    use Setter;

    /**
     * 模块编号
     * @var
     */
    protected $model;


    /**
     * Data Access Object
     * @Object
     */
    protected $service;


    /**
     * 表单数据
     * @var array
     */
    protected $inputs = array();


    /**
     * 执行结果验证
     *
     * @param bool $result dao对象返回值
     * @return void
     * @throws \think\Exception
     */
    protected function execute($result = false): void
    {
        $model = $this->model ?? 10;
        $result = is_numeric($result) ? intval($result) : $result;

        if ($result > 0) {
            $statusCode = 0;
            $this->setStatusCode($statusCode);
        } elseif (0 === $result) {
            $statusCode = intval('1' . $model . '008');
            $this->setStatusCode($statusCode); // 数据未更新
        } else {
            $statusCode = intval('1' . $model . '009');
            $this->setStatusCode($statusCode); // 数据库异常
        }
    }

    /**
     * 依赖注入
     *
     * @param object $dao  数据访问对象
     * @return object $dao
     */
    protected function setter(object $dao): object
    {
        $this->service = $dao;
        return $dao;
    }

    /**
     * 全局结果返回
     *
     * @Description 全局结果返回会将父类构造器初始化值及parameters属性值进行返回
     * @param array $mVal 返回数据
     * @return array
     */
    protected function returnRes(array $mVal = []): array
    {
        return array_merge($this->parameters, (array)$mVal);
    }

    /**
     * 删除空（筛选）条件
     *
     * @Description 筛选条件中过滤空条件
     * @param array $inputs
     * @return array
     */
    protected function rmEmpty(array $inputs = []): array
    {
        array_walk($inputs, function ($valut, $key) use (&$inputs) {
            if ('' === $valut) {
                unset($inputs[$key]);
            }
        });

        return (array)$inputs;
    }

    /**
     * 数据差异对比
     *
     * @param array $origin 原始的老旧数据
     * @param array $inputs 输入数据
     * @return $this
     */
    protected function meld(array $origin = [], array $inputs = [])
    {
        $diff = array();
        $inputs = $inputs ?: $this->inputs;

        if ($inputs) {
            array_walk($inputs, function ($value, $key) use (&$diff, $origin) {
                if ((string)$value !== strval($origin[$key])) {
                    $diff[$key] = $value;
                }
            });

            $this->inputs = (array)$diff;
        }

        return $this;
    }

    /**
     * 数据更新逻辑
     *
     * @description meld操作支持链式，默认将差异数据保存在input属性中。也可以跳过meld操作，自定义更新的数据
     * @param array $where  更新条件
     * @param array $inputs 输入数据
     * @param object $dao   数据访问对象（依赖注入）
     * @return $this
     */
    protected function update(array $where = [], array $inputs = [], ?object $dao)
    {
        $dao = is_object($dao) ? $dao : $this->service;
        $mapping = $inputs ?: $this->inputs;  // 将数据差异数据进行映射

        // 差量更新
        if ($mapping && !empty($mapping)) {
            // 存在差异数据且表单数据合法时才执行更新
            $this->execute($dao->save($mapping, $where));
        } else {
            $this->execute(0);
        }

        return $this;
    }

    /**
     * 更新缓存
     *
     * @Description 缓存标示符与指定格式字符串拼接成 cache-Key
     * @param string $pattern 缓存标示符
     * @param array  $inputs  输入数据
     * @param object $dao　　　数据访问对象（依赖注入）
     * @return void
     */
    protected function upcache(string $pattern, array $inputs = [], object $dao = NULL, bool $asyn = false): void
    {
        $dao = is_object($dao) ? $dao : $this->service;
        $data = $inputs ?: $this->inputs;

        // 存在差异数据时才更新缓存
        if ($data) {
            if (true === $asyn) {

            } else {
                $dao->upCache((array)$data, strval($pattern));
            }
        }
    }

    /**
     * 析构释放内存
     */
    public function __destruct()
    {
        unset($this->service, $this->inputs);
    }

    /**
     * 初始化构造器
     * @param array $data
     */
    public function __construct(array $options = [])
    {
        $this->parameters = $options;
        $this->setCode(300);
    }
}
