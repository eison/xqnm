<?php
// +----------------------------------------------------------------------+
// | 数据库 DAO 公共模型
// +----------------------------------------------------------------------+
namespace libraries\midware;

use libraries\helper\Redis;
use think\Model;

class DaoModel extends Model
{
    /**
     * 缓存对象
     * @var null
     */
    protected $cache = Null;


    /**
     * 缓存关系映射联表
     * @var array
     */
    protected $keyMap;


    /**
     * 数据库非法字段过滤
     * @var
     */
    protected $filter;


    /**
     * 缓存对象注入
     *
     * @param object $cache
     * @return void
     */
    protected function initialize(object $cache = NULL)
    {
        parent::initialize();

        $this->cache = is_object($cache) ? Redis::getInstanc($cache) : Redis::getInstanc();
    }

    /**
     * 时间格式字段拆分
     *
     * @param string $key 字段名称
     * @param string $value 字段值
     * @return array
     */
    protected function explodeDate(string $key, string $value)
    {
        if (stripos($value, '::') > -1) {
            list($start, $end) = explode('::', $value);
            $map[$key] = array('between', [specif_time($start), specif_time($end)]);
        } else {
            $map[$key] = (string)$value;
        }

        return $map;
    }

    /**
     * 统一数据库保存保存Redis标识符
     *
     * @param string $pattern 缓存唯一标识符
     * @return string
     */
    protected function symbol(string $pattern): string
    {
        $pattern = trim($pattern);
        return "{$this->name}:{$pattern}";
    }

    /**
     * 查询条件过滤非法查询字段
     *
     * @param array $condition 查询条件
     * @param bool $flat 日期时间过滤
     * @return array 查询条件
     */
    public function filter()
    {
        $this->filter = true;
        return $this;

        $condition =  is_array($condition) ? $condition : (array)$condition;
        $tableFields = $this->getTableFields(); // 获取数据表结构字段

        if ($condition) {
            if (true === $flat) {
                $timeFields = $this->getTimeFields(); // 获取时间字段
            }

            foreach ($condition as $field => $value) {
                if (!in_array((string)$field, $tableFields)) {
                    unset($condition[$field]);  // 过滤查询条件
                }

                if (true === $flat &&
                    (in_array($field, $timeFields) && stripos($value, '::') > -1)
                ) {
                    $condition = array_merge($condition, $this->explodeDate($field, $value));
                }
            }
        }

        return (array)$condition;
    }

    /**
     * 动态维护(同步)缓存
     *
     * @Description 缓存标示符与指定格式字符串拼接成 cache-Key 作为缓存保存键
     * @param array $inputs 　输入数据
     * @param string $pattern 缓存标示符
     * @return void
     */
    public function upCache(array $inputs, string $pattern): void
    {
        $key = $this->symbol($pattern);
        $cacheData = $this->cache->get($key, false);

        // 并发下缓存失效则不更新，等下次缓存时再进行更新
        if ($cacheData) {
            if ($this->keyMap) {
                foreach (__yield(count($this->keyMap)) as $index) {
                    array_walk($this->keyMap[$index], function ($field) use (&$cacheData, $inputs) {
                        $pattern = $this->symbol($cacheData[$field]);
                        $keysMap = $this->cache->seachKey('*' . $pattern . '*'); // 索引缓存保存键

                        if ($keysMap) {
                            foreach ($keysMap as $key) {
                                $result = $this->cache->set($key, array_merge($cacheData, $inputs));
                                !$result and $this->cache->delete($key);  // 若缓存更新失败则删除缓存
                            }
                        }
                    });
                }
            } else {
                $result = $this->cache->set($key, array_merge($cacheData, $inputs));
                !$result and $this->cache->delete($key);  // 若缓存更新失败则删除缓存
            }
        }
    }
}
