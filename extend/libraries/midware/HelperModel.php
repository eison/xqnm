<?php
// +----------------------------------------------------------------------+
// | 基于逻辑模型与数据模型的中间层抽象
// +----------------------------------------------------------------------+
// | data：2019-04-29 15:01:18
// +----------------------------------------------------------------------+
namespace libraries\midware;

use \think\Model;

abstract class HelperModel extends Model
{
    use CommonModel;

    /**
     * 全局参数
     * @var
     */
    public $parameters;


    /**
     * 获取字段类型为日期时间的字段
     * @return array
     */
    private function getTimeFields()
    {
        foreach ($this->getFieldsType() as $fields => $fieldType) {
            if ('datetime' == $fieldType) {
                $timeFields[] = $fields;
            }
        }

        return $timeFields;
    }

    /**
     * 初始化构造器
     * @param array $data
     */
    protected function initialize($data = [])
    {
        parent::initialize($data);
        $this->setCode(0);
    }

    /**
     * 全局统一参数设置
     *
     * @param string $parameterKey      key键
     * @param mixed  $mParameterValues  value值
     * @return void
     */
    protected function setParameter($parameterKey, $mParameterValues)
    {
        $this->parameters[trim($parameterKey)] = trim($mParameterValues);
    }

    /**
     * 全局结果返回
     * @Description: 全局结果返回会将父类构造器初始化值及_parameters属性值进行返回
     *
     * @param array $mVal
     * @return array
     */
    protected function returnRes(array $mVal = [])
    {
        return array_merge($this->parameters, (array)$mVal);
    }

    /**
     * 时间格式字段拆分
     *
     * @param $key     字段名称
     * @param $value   字段值
     * @return array
     */
    protected function explodeDate($key, $value)
    {
        $map = array();

        if (stripos($value, '::') > -1) {
            list($start, $end) = explode('::', $value);
            $start_time=specif_time($start) . ' ' . '00:00:00';
            $end_time=specif_time($end). ' ' . '23:59:59';
            $map[$key] = array('between', [$start_time,$end_time]);
        } else {
            $map[$key] = (string)$value;
        }

        return $map;
    }

    /**
     * 查询条件过滤非法查询字段
     *
     * @param array $condition   查询条件
     * @param bool  $flat        日期时间过滤
     * @return array             返回复合插叙语句
     */
    protected function filter(array $condition = [], $flat = false)
    {
        $condition = is_array($condition) ? $condition : (array)$condition;
        $tableFields = $this->getTableFields();         // 获取数据表结构字段

        if ($condition) {
            if (true === $flat) {
                $timeFields = $this->getTimeFields();  // 获取时间字段
            }

            foreach ($condition as $field => $value) {
                if (!in_array((string)$field, $tableFields)) {
                    unset($condition[$field]);          // 过滤查询条件
                }

                if (true === $flat &&
                    (in_array($field, $timeFields) && stripos($value, '::') > -1)
                ) {
                    $condition = array_merge($condition, $this->explodeDate($field, $value));
                }
            }
        }

        return (array)$condition;
    }
}
