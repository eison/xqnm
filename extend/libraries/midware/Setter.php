<?php
// +----------------------------------------------------------------------+
// | 性状 -- Setter
// +----------------------------------------------------------------------+
// | data：2019-05-15 15:29:39  by eison
// +----------------------------------------------------------------------+
namespace libraries\midware;

trait Setter
{
    /**
     * 全局参数
     * @array
     */
    public $parameters;


    /**
     * 全局统一参数设置
     *
     * @param string $parameterKey      key键
     * @param mixed  $mParameterValues  value值
     * @return void
     */
    public function setParameter(string $key, $value)
    {
        $this->parameters[trim($key)] = $value;
    }

    /**
     * 设置请求状态码
     *
     * @param int $code  请求状态吗 (200 | 300)
     * @return void
     */
    public function setCode(int $code)
    {
        $this->parameters['code'] = (int)$code;
    }

    /**
     * 获取请求状态码
     * @return int
     */
    public function getCode()
    {
        return (int)$this->parameters['code'] ?? 300;
    }

    /**
     * 设置错误状态码
     *
     * @param int $code  六位数的错误码
     * @return void
     */
    public function setStatusCode(int $code)
    {
        $this->parameters['statusCode'] = (int)$code;
    }

    /**
     * 获取错误状态码
     * @return int
     */
    public function getStatusCode()
    {
        return (int)$this->parameters['statusCode'] ?? 110001;
    }

    /**
     * 设置错误提示语
     *
     * @param string $message  错误提示语
     * @return void
     */
    public function setMessage(string $message)
    {
        $this->parameters['msg'] = (trim($message));
    }

    /**
     * 设置返回的数组数据
     *
     * @param array $message  需要返回的数组数据
     * @return void
     */
    public function setRetData(array $data)
    {
        $this->parameters['data'] = (array)$data;
    }
}
