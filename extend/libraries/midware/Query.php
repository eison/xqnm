<?php
// +----------------------------------------------------------------------+
// | \think\db\Query 类扩充函数库
// +----------------------------------------------------------------------+
// | data：2019-04-28 17:49:58
// +----------------------------------------------------------------------+
namespace libraries\midware;

trait Query
{
    /**
     * 复合查询语句条件副本
     * @var array
     */
    public $cpWhere = array();

    /**
     * 分页总条数
     * @var int
     */
    protected $pageTotalRows = 0;


    /**
     * 获取总数据长度
     *
     * @param void
     * @return void
     */
    protected function totalRows()
    {
        $this->pageTotalRows = (int)$this->where($this->cpWhere)->count();
    }

    /**
     * 获取分页域
     *
     * @param void
     * @return array
     */
    protected function pageScope()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $limit = isset($_REQUEST['limit']) ? intval($_REQUEST['limit']) : 1;

        return array($page, $limit);
    }

    /**
     * 获取分页数据列及总数据长度
     *
     * @param int $page  当前显示页数
     * @param int $limit 每页限制条数
     * @return array     未获取到数据时返回空数组，获取到数据时返回数据列及总数据长度
     */
    public function pagination($page = NULL, $limit = NULL)
    {
        if (is_null($page) && is_null($limit)) {
            list($page, $limit) = $this->pageScope();
        }

        $start = ($page - 1) * $limit;
        $lists = $this->limit($start, (int)$limit)->select();

        if ($lists) {
            $lists = collection($lists)->toArray();
            $this->totalRows();
        }

        return [(array)$lists, $this->pageTotalRows];
    }
}
