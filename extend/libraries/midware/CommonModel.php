<?php
// +----------------------------------------------------------------------+
// | \think\db\Query 类扩充函数库
// +----------------------------------------------------------------------+
// | data：2019-04-28 17:49:58
// +----------------------------------------------------------------------+
namespace libraries\midware;

trait CommonModel
{
    /**
     * 通过指定条件获取一条纪录
     *
     * @param array $conditons  查询条件
     * @return null | false | array
     */
    public function getInfo_By_Cond(array $conditons)
    {
        $conditons = $this->filter($conditons);
        $rowInfo = $this->where($conditons)->find();
        return is_null($rowInfo) ? $rowInfo : $rowInfo->toArray();
    }

    /**
     * 通过指定条件判断纪录是否存在
     *
     * @param array $conditons  查询条件
     * @return true | false
     */
    public function hasExist_By_Cond(array $conditons)
    {
        $conditons = $this->filter($conditons);
        $primaryKey = $this->where($conditons)->column($this->pk);
        return $primaryKey ? true : false;
    }

    /**
     * 获取商家账户金额变动纪录
     *
     * @param array $conditons      查询条件
     * @return mixed
     * date:2019-04-28 11:59:54  by eison
     */
    public function getList(array $conditons)
    {
        $conditons = $this->filter($conditons, true);
        list($list, $count) = $this->where($conditons)->order($this->pk, 'desc')->pagination();

        if ($list && $count > 0) {
            $this->setParameter('statusCode', '001');
        } else {
            $this->setParameter('statusCode', '010');
        }

        return $this->returnRes(['data' => $list, 'count' => $count]);
    }
}
