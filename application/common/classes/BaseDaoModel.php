<?php
// +----------------------------------------------------------------------+
// | 公共模块 -- 基础逻辑模型
// +----------------------------------------------------------------------+
// | date：2019-05-13 15:24:14  by eison
// +----------------------------------------------------------------------+
namespace app\common\classes;

use libraries\midware\DaoModel;

class BaseDaoModel extends DaoModel
{
    /**
     * 新增数据时自动完成
     * @var array
     */
    protected $insert = ['addtime'];


    /**
     * 更新或者新增数据时自动完成
     * @var array
     */
    protected $auto = ['uptime'];


    /**
     * 新增数据的时候自动插入时间
     * @return false|string
     */
    protected function setAddTimeAttr()
    {
        return format_time();
    }

    /**
     * 更新或新增数据的时候自动更新时间
     * @return false|string
     */
    protected function setUpTimeAttr()
    {
        return format_time();
    }

    /**
     * 通过 主键id 获取表纪录信息
     *
     * @param string $primaryId 主键id
     * @return mixed
     * date：2019-05-15 16:00:14  by eison
     */
    public function getInfo_byId(?int $primaryId)
    {
        $field = 'getby' . $this->getPk();
        $info = $this->$field((int)$primaryId);

        return $info ? $info->toArray() : NULL;
    }
}
