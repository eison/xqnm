<?php
// +----------------------------------------------------------------------+
// | 数据模型 -- 店铺表
// +----------------------------------------------------------------------+
namespace app\common\dao;

use app\common\classes\BaseDaoModel;

class Shop extends BaseDaoModel
{
    /**
     * 获取以shopId为key，shopName为value的店铺列表
     *
     * @param int $customerId 客户Id
     * @return mixed 打包到一维索引数组返回
     * date：2019-06-15 10:40:41  by eison
     */
    public function scopeGetIdKey_NameValues(?object $query, ?int $customerId)
    {
        $condition = array('customerId' => (int)$customerId);
        $result = $query->where($condition)->column('shopName', 'shopId');

        return $result ?: NUll;
    }
}
