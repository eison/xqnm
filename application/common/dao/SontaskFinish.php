<?php
// +----------------------------------------------------------------------+
// | 数据模型 -- 子任务完成表
// +----------------------------------------------------------------------+
namespace app\common\dao;

use app\common\classes\BaseDaoModel;

class SontaskFinish extends BaseDaoModel
{
    /**
     * 通过条件获取商家任务列表
     *
     * @param array $condition 查询条件
     * @return array
     * date：2019-06-12 18:35:52  by eison
     */
    public function getList_byCond(?array $condition): array
    {
        $result = $this->where((array)$condition)->order('sontask_status_time asc')->pagination();
        return $result;
    }
}
