<?php
// +----------------------------------------------------------------------+
// | 数据模型 -- 商户账户表
// +----------------------------------------------------------------------+
namespace app\common\dao;

use app\common\classes\BaseDaoModel;

class ShopUser extends BaseDaoModel
{
    /**
     * 数据字段常量配置
     * @var array
     */
    public $const = [
        // 商家账号状态
        'shop_user_limit' => [
            'enabled'  => 0,    // 正常
            'disabled' => 1,    // 禁用
            'Unaudit'  => 2     // 未审核
        ],

        // 删除状态
        'shop_user_delete_status' => [
            'enabled'  => 0,     // 正常
            'disabled' => 1,     // 删除
        ],
    ];

    /**
     * 通过用户名字获取管理员信息
     *
     * @param string $shop_user_name 商家用户名
     * @return mixed
     * date：2019-06-14 09:28:59  by eison
     */
    public function getInfo_byName(?string $shop_user_name)
    {
        $info = $this->getbyShopUserName($shop_user_name);
        return $info ? $info->toArray() : NULL;
    }

    /**
     * 获取用户账户可用余额
     *
     * @param int $shop_user_id  商家用户id
     * @return int
     * date：2019-06-19 14:49:47  by eison
     */
    public function getAvailBalance(?int $shop_user_id)
    {
        $balance = 0;
        $info = $this->getInfo_byId($shop_user_id);

        if ($info) {
            $balance =  $info['credit_granting']
                ? $info['account_money'] + ($info['line_credit'] - $info['credit_quota'])
                : $info['account_money'];
        }

        return $balance;
    }
}
