<?php
// +----------------------------------------------------------------------+
// | 数据模型 -- 子任务表
// +----------------------------------------------------------------------+
namespace app\common\dao;

use app\common\classes\BaseDaoModel;

class Sontask extends BaseDaoModel
{
    /**
     * 通过用户名字获取管理员信息
     *
     * @param string $manage_name 用户名
     * @return mixed $info 有数据时返回数组，无数据时返回null
     * date：2019-05-14 09:28:59  by eison
     */
    public function getInfo_byName(?string $shop_user_name)
    {
        $info = $this->where(['shop_user_name' => $shop_user_name])->find();
        return $info ? $info->toArray() : NULL;
    }
}
