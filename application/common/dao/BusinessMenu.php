<?php
// +----------------------------------------------------------------------+
// | 数据模型 -- 商家后台权限菜单表
// +----------------------------------------------------------------------+
namespace app\common\dao;

use app\common\classes\BaseDaoModel;

class BusinessMenu extends BaseDaoModel
{
    /**
     * 获取商家后台权限菜单列表
     *
     * @return mixed
     * date：2019-06-12 16:02:24  by eison
     */
    public function getMenus()
    {
        $condition = array('business_menu_delete_status' => 0);
        $list = $this->where($condition)->order('sequence asc')->select();

        return $list ? collection($list)->toArray() : NULL;
    }
}
