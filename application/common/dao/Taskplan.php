<?php
// +----------------------------------------------------------------------+
// | 数据模型 -- 任务计划表
// +----------------------------------------------------------------------+
namespace app\common\dao;

use app\common\classes\BaseDaoModel;

class Taskplan extends BaseDaoModel
{
    /**
     * 新增数据时自动完成
     * @var array
     */
    protected $insert = ['create_time'];


    /**
     * 更新或者新增数据时自动完成
     * @var array
     */
    protected $auto = ['update_time'];


    /**
     * 新增数据的时候自动插入时间
     * @return false|string
     */
    protected function setCreateTimeAttr()
    {
        return format_time();
    }

    /**
     * 更新或新增数据的时候自动更新时间
     * @return false|string
     */
    protected function setUpdateTimeAttr()
    {
        return format_time();
    }
}
