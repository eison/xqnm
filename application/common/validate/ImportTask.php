<?php
// +----------------------------------------------------------------------+
// | 验证器 -- 导单
// +----------------------------------------------------------------------+
namespace app\common\validate;

use think\Validate;

class ImportTask extends Validate
{
    /**
     * 验证规则
     * @var array
     */
    protected $rule = [
        'taskDate'       => 'date',                         // 任务日期
        'shopName'       => 'checkshop',                    // 店铺名
        'taskbadyLink'   => 'require|checkurl|checkformat', // 宝贝链接
        'mainFigureLink' => 'require|checkurl',             // 主图链接
        'customerOrder'  => 'require|float|max:1000',       // 客单
        'taskType'       => 'require',                      // 类型
        'taskKeyWord'    => 'require',                      // 关键字
        'timeSlot'       => 'require|speciftimeslot',       // 时间段
        'collectDate'    => 'speciftime',                   // 浏览任务收藏时间
    ];

    /**
     * 错误提示
     * @var array
     */
    protected $message = [
        'taskDate.date'            => '第 :sentence 行, 任务时间日期格式错误',
        'shopName.checkshop'       => '第 :sentence 行, 店铺名称错误',
        'taskbadyLink.require'     => '第 :sentence 行, 商品连接不能为空',
        'taskbadyLink.checkurl'    => '第 :sentence 行, 宝贝连接格式不合法',
        'taskbadyLink.checkformat' => ':sentence',
        'mainFigureLink.require'   => '第 :sentence 行, 主图连接不能为空',
        'mainFigureLink.checkurl'  => '第 :sentence 行, 主图连接不合法, 要带 http 或 https 协议标识',
        'customerOrder.require'    => '第 :sentence 行, 客单金额不能为空',
        'customerOrder.float'      => '第 :sentence 行, 客单金额格式不合法',
        'customerOrder.max'        => '第 :sentence 行, 客单金额不能超过1000',
        'taskType.require'         => '第 :sentence 行, 类型不能为空',
        'taskKeyWord.require'      => '第 :sentence 行, 关键词不能为空',
        'timeSlot.require'         => '第 :sentence 行, 时间段不能为空',
        'timeSlot.speciftimeslot'  => '第 :sentence 行, 时间段格式错误。请参考范例格式。多个时间段用英文符;分割',
        'collectDate.date'         => '第 :sentence 行, 浏览任务收藏时间日期格式错误',
    ];

    /**
     * 验证店铺名称
     *
     * @param $value 验证参数
     * @return bool
     */
    protected function checkshop($value)
    {
        $shopList = session('shop_user.shop_list');
        return in_array(trim($value), $shopList);
    }

    /**
     * 验证 商品连接 url 的合法性
     *
     * @param $value 验证参数
     * @return bool
     */
    protected function checkurl($value)
    {
        $result = preg_match('/^http[s]?:\/\/[^\s]*/', $value);
        return $result ? true : false;
    }

    /**
     * 验证商品连接主图格式
     *
     * @param $value
     * @return bool
     */
    protected function checkformat($value, ...$params)
    {
        $arguments = (array)$params;

        if (strpos($value, "#") > -1) {
            $this->setSentence('第 ' . end($arguments) . ' 行, 商品连接错误, 存在#detail');
            return false;
        }

        if (strpos($value, "&") > -1) {
            if (preg_match('/^&.*/', explode('&', $value)[1])) {
                $this->setSentence('第' . end($arguments) . '行, 主图链接?后不能直接带&');
                return false;
            }
        }

        return true;
    }

    /**
     * 验证任务时间段格式
     *
     * @param $value
     * @return bool
     */
    protected function speciftimeslot($value)
    {
        if (stripos($value, ';') > 0) {
            $rule = '/((^[0-1][0-9]|^2[0-3]):([0-5][0-9]))-(0([1-9])+|[1-9]([0-9])*)$/';
            $result = true;

            // 采用正则集合验证不准确,必须迭代验证
            foreach ((explode(';', $value)) as $timeslot) {
                if ($timeslot && !preg_match($rule, trim($timeslot))) {
                    $result = false;
                    break;
                }
            }
        }

        return $result ?? false;
    }
}
