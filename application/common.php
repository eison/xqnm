<?php
// +----------------------------------------------------------------------+
// | 公共函数库
// +----------------------------------------------------------------------+

/**
 * 生成随机数
 *
 * @param int  $length    步长（生成随机数长度）
 * @param bool $numeric   是否生成存数字随机数
 * @return string
 * date：2019-05-15 16:47:14  by eison
 */
function create_random($length = 6, $isNumeric = true) {
    PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);

    if ($isNumeric) {
        $hash = sprintf('%0' . $length . 'd', mt_rand(0, pow(10, $length) - 1));
    } else {
        $hash = '';
        $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ123456789abcdefghjkmnpqrstuvwxyz';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }
    }

    return $hash;
}

/**
 * 获取商家账户id
 *
 * @param void
 * @return int
 * date：2019-06-11 15:40:56 by eison
 */
function get_shop_user_id() {
    $shopUserInfo = get_shop_user();
    return (int)$shopUserInfo['shop_user_id'] ?? 0;
}

/**
 * 获取商家账户信息
 *
 * @param void
 * @return mixed
 * date：2019-06-12 17:24:33 by eison
 */
function get_shop_user() {
    return session('shop_user.info');
}

/**
 * 步长迭代器
 *
 * @param int $start 初始值
 * @param int $limit 长度
 * @return not return
 * date：2019-05-18 16:53:27  by eison
 */
function __yield($limit, $start = 0) {
    for ($number = $start; $number < $limit; $number++) {
        yield $number;
    }
}

function formatTime($date) {
	$str = '';
	$timer = strtotime($date);
	$diff = $_SERVER['REQUEST_TIME'] - $timer;
	$day = floor($diff / 86400);
	$free = $diff % 86400;
	if($day > 0) {
		return $day."天前";
	}else{
		if($free>0){
			$hour = floor($free / 3600);
			$free = $free % 3600;
			if($hour>0){
				return $hour."小时前";
			}else{
				if($free>0){
					$min = floor($free / 60);
					$free = $free % 60;
						if($min>0){
							return $min."分钟前";
						}else{
							if($free>0){
								return $free."秒前";
							}else{
								return '刚刚';
						}
					}
				}else{
					return '刚刚';
				}
			}
		}else{
			return '刚刚';
		}
	}
}

function unescape($str) { 
    $ret = ''; 
    $len = strlen($str); 
    for ($i = 0; $i < $len; $i ++) 
    { 
        if ($str[$i] == '%' && $str[$i + 1] == 'u') 
        { 
            $val = hexdec(substr($str, $i + 2, 4)); 
            if ($val < 0x7f) 
                $ret .= chr($val); 
            else  
                if ($val < 0x800) 
                    $ret .= chr(0xc0 | ($val >> 6)) . 
                     chr(0x80 | ($val & 0x3f)); 
                else 
                    $ret .= chr(0xe0 | ($val >> 12)) . 
                     chr(0x80 | (($val >> 6) & 0x3f)) . 
                     chr(0x80 | ($val & 0x3f)); 
            $i += 5; 
        } else  
            if ($str[$i] == '%') 
            { 
                $ret .= urldecode(substr($str, $i, 3)); 
                $i += 2; 
            } else 
                $ret .= $str[$i]; 
    } 
       return $ret; 
    }


function getbadyLink($baby_url) {
	$ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL,	$baby_url);
    curl_setopt ($ch, CURLOPT_POST, FALSE);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt ($ch, CURLOPT_TIMEOUT, '30');
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)");
    curl_setopt ($ch, CURLOPT_AUTOREFERER, TRUE);
    $response = curl_exec($ch);
    $dturl = strstr($response, "var url = '");
    $arr =explode("'",$dturl);
    curl_close($ch);
    return $arr[1];
}

/** 
 * Returns the url query as associative array 
 * 参数的截取
 * @param    string    query 
 * @return    array    params 
 */
function convertUrlQuery($query) {
    $queryParts = explode('&', $query); 
     
    $params = array(); 
    foreach ($queryParts as $param) 
    { 
        $item = explode('=', $param); 
        $params[$item[0]] = $item[1]; 
    } 
    return $params; 
}

//random() 函数返回随机整数。
function verification_random($length = 6, $numeric = 0) {
    PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);

    if ($numeric) {
        $hash = sprintf('%0' . $length . 'd', mt_rand(0, pow(10, $length) - 1));
    } else {
        $hash = '';
        $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ123456789abcdefghjkmnpqrstuvwxyz';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }
    }

    return $hash;
}

/**
 * author fenghao@ipoxiao.com
 * date 2015/07/17 18:50
 * 替换银行卡、手机号码为**。
 * @param type $str 要替换的字符串
 * @param type $startlen 开始长度 默认4
 * @param type $endlen 结束长度 默认3
 * @return type
 */
function strreplace($str, $startlen = 4, $endlen = 4) {
    $repstr = "";
    if (strlen($str) < ($startlen + $endlen + 1)) {
        return $str;
    }
    $count = strlen($str) - $startlen - $endlen;
    for ($i = 0; $i < $count; $i++) {
        $repstr .= "*";
    }
    return preg_replace('/(\d{' . $startlen . '})\d+(\d{' . $endlen . '})/', '${1}' . $repstr . '${2}', $str);
}
/**
 * author fenghao@ipoxiao.com
 * date 2015/07/17 18:50
 * 隐藏姓名为**。
 * @param type $str 要替换的字符串
 * @param type $startlen 开始长度 默认4
 * @param type $endlen 结束长度 默认3
 * @return type
 */
function substr_cut($user_name) {
    $strlen   = mb_strlen($user_name, 'utf-8');
    $firstStr = mb_substr($user_name, 0, 1, 'utf-8');
    $lastStr  = mb_substr($user_name, -1, 1, 'utf-8');

    return $strlen == 2
        ? $firstStr . str_repeat('*', mb_strlen($user_name, 'utf-8') - 0)
        : $firstStr . str_repeat("*", $strlen - 2) . $lastStr;
}

/**
 * 打印调试函数
 * date：2019-04-18 14:26:21
 */
function p(...$params) {
    foreach ($params as $index => $value) dump($value);
    exit;
}

/**
 * 用户登录密码加密函数
 * date：2019-04-26 16:25:32  by eison
 */
function passwd_hash($value) {
    return MD5($value);
}

/**
 * 返回统一时间格式
 * date：2019-04-26 16:25:23  by eison
 */
function format_time() {
    return date('y-m-d H:i:s', time());
}

/**
 * 表单数据日期规范化
 * date：2019-04-28 18:08:11  by eison
 */
function specif_time($time = NULL) {
    static $speciftime;

    if (!$speciftime) {
        $time = $time ?: date('Y-m-d', time());
        $speciftime = date('Y-m-d', strtotime($time));
    }

    return $speciftime;
}

/*
 * 生成uuid
 * date：2019-05-02 13:27:30  by eison
 */
function create_uuid() {
    if (function_exists('com_create_guid')) {
        return com_create_guid();
    } else {
        mt_srand((double)microtime() * 10000);
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);
        $uuid = substr($charid, 0, 8) . $hyphen
            . substr($charid, 8, 4)   . $hyphen
            . substr($charid, 12, 4)  . $hyphen
            . substr($charid, 16, 4)  . $hyphen
            . substr($charid, 20, 12);

        return $uuid;
    }
}
