<?php
// +----------------------------------------------------------------------
// | 商家模块 -- 表单数据库字段映射
// +----------------------------------------------------------------------
return [
    # 账号管理
    'Account' => [
        // 登录
        'doLogin' => [
            'shop_user_name' => 'shopName',         // 账号
            'shop_user_password' => 'shopPassword', // 密码
        ]
    ],

    # 任务管理
    'Sontask' => [
        // 获取任务管理列表
        'getList'  => [
            'shop_id' => 'shopId',                  // 店铺id
            'shop_name' => 'searchshopName',        // 店铺名称
            'apprenticewwnumber' => 'wwnumber',     // 旺旺号
            'apprentice_ordernum' => 'ordernumber', // 订单号
            'sontask_finish_time' => 'time',        // 任务时间
        ],

        // 支付计划费用
        'checkout' => [
            'plan_sex' => 'sex_value',
            'plan_region' => 'region_limit',
            'plan_device' => 'device_value',
            'task_set_num' => 'set_task_num',
            'plan_shop_id' => 'shopId',
            'plan_shop_name' => 'shopName',
            'plan_reputation' => 'rank_value',
        ],
    ],
];
