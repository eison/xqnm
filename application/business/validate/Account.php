<?php
// +----------------------------------------------------------------------+
// | 验证器 -- 账号管理
// +----------------------------------------------------------------------+
namespace app\business\validate;

use think\Validate;

class Account extends Validate
{
    /**
     * 验证规则
     * @var array
     */
    protected $rule = [
        'doLogin' => [
            'shopName'     => 'require',
            'shopPassword' => 'require',
        ],
    ];

    /**
     * 错误提示
     * @var array
     */
    protected $message = [
        'doLogin' => [
            'shopName.require'     => '用户名不能为空',
            'shopPassword.require' => '密码不能为空',
        ],
    ];
}




