<?php
// +----------------------------------------------------------------------+
// | 商家后台模块 -- 基础控制器
// +----------------------------------------------------------------------+
// | date：2019-06-11 15:16:39
// +----------------------------------------------------------------------+
namespace app\business\classes;

use libraries\midware\MidwareController;

class BaseController extends MidwareController
{
    /**
     * 非登录状态允许访问的 api action
     * @var array
     */
    protected $openAPIs = ['login', 'doLogin', 'register', 'doRegister'];


    /**
     * 登录拦截
     */
    protected function intercept()
    {
        if (0 == get_shop_user_id()) {
            $routes = explode('/', $this->request->pathinfo());
            $action = $routes[2] ?? 'index';

            if (!in_array($action, $this->openAPIs)) {
                if ($this->request->isAjax()) {
                    $this->setMessage('请登录');
                    $this->returnRes();
                } else {
                    $this->redirect("/business/Account/login");
                }
            }
        }
    }

    /**
     * 初始化构造器
     * @return void
     */
    protected function _initialize()
    {
        parent::_initialize();
        $this->intercept(); // 登录拦截
    }
}
