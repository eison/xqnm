<?php
// +----------------------------------------------------------------------+
// | 商家后台模块 -- 逻辑基础模型
// +----------------------------------------------------------------------+
namespace app\business\classes;

use libraries\midware\LogicModel;
use think\Loader;

class BaseLogic extends LogicModel
{
    /**
     * 模块编号
     * @var int
     */
    protected $model = 12;


    /**
     * 获取商家所有的店铺
     *
     * @description 消息队列进行优化
     * @param int $customerId 客户Id
     * @return void
     */
    protected function getShops($customerId): void
    {
        $shopList = Loader::dao('Shop')->getIdKey_NameValues($customerId, true);
        \think\Session::set('shop_user.shop_list', $shopList);
    }

    /**
     * 初始化构造器
     * @param array $data
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);
        $this->setStatusCode(112001 /*缺少重要参数*/);  // 初始化错误
    }
}
