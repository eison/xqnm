<?php
// +----------------------------------------------------------------------+
// | 控制器 -- 任务管理
// +----------------------------------------------------------------------+
namespace app\business\controller;

use app\business\classes\BaseController;
use think\Loader;
use app\shop\model\Sontaskmodel;

class Sontask extends BaseController
{
    private function _validate($datas)
    {
        $validate = Loader::validate('business/Sontask');
        $result = $validate->check($datas);

        if (!$result) {
            $this->setStatusCode(112007/*数据格式错误*/);
            $this->setMessage($validate->getError());

            $this->returnRes();
        }
    }

    /**
     * 获取任务管理列表
     */
    public function getList()
    {
        if ($this->request->isGet()) {
            $datas = $this->request->only(['shopId', 'wwnumber', 'ordernumber', 'time']);
            $inputs = $this->convert($datas);
            $results = Loader::logic('Sontask')->getList($inputs);

            return $this->returnRes($results);
        }
    }

    /**
     * 导入任务
     */
    public function importTask()
    {
        if ($this->request->isPost()) {
            $binary = $this->request->file('filename');

            if (!empty($binary)) {
                $results = Loader::logic('Sontask')->importTask($binary);  // 上传的二级制文件
                return $this->returnRes($results);
            }
        }
    }

    /**
     * 支付计划费用
     */
    public function checkout()
    {
        if ($this->request->isPost()) {
            $datas = $this->request->only(['data', 'set_arr']);
            $inputs = array_merge($datas, $this->convert($datas['set_arr']));

            unset($inputs['set_arr']);
            $results = Loader::logic('Sontask')->checkout($inputs);

            return $this->returnRes($results);
        }
    }
}
