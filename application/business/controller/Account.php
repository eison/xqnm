<?php
// +----------------------------------------------------------------------+
// | 控制器 -- 账号管理
// +----------------------------------------------------------------------+
namespace app\business\controller;

use app\business\classes\BaseController;
use think\Loader;

class Account extends BaseController
{
    private function _validate($action, $datas)
    {
        $validate = Loader::validate('business/Account');
        $result = $validate->action($action)->check($datas);

        if (!$result) {
            $this->setStatusCode(112007 /*通用数据格式错误*/);
            $this->setMessage($validate->getError());

            $this->returnRes();
        }
    }

    /**
     * 执行登录
     */
    public function doLogin()
    {
        if ($this->request->isPost()) {
            $datas = $this->request->only(['shopName', 'shopPassword']);

            $this->_validate('doLogin', $datas);
            $inputs = $this->convert($datas);
            $results = Loader::logic('Account')->doLogin($inputs);

            return $this->returnRes($results);
        }
    }

    /**
     * 执行登出
     */
    public function doLogout()
    {
        if ($this->request->isPost()) {
            \think\Session::delete('shop_user.info');
            \think\Session::destroy();

            $this->setMessage('您的账号已经安全登出');
            return $this->returnRes(['code' => 200, 'statusCode' => 0]);
        }
    }
}
