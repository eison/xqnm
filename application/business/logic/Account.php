<?php
// +----------------------------------------------------------------------+
// | 逻辑模型 -- 账号管理
// +----------------------------------------------------------------------+
namespace app\business\logic;

use app\business\classes\BaseLogic;
use think\Loader;

class Account extends BaseLogic
{
    /**
     * 商家登录
     *
     * @param array $inputs 表单输入数据
     * @return array
     * date：2019-05-14 11:36:43  by  eison
     */
    public function doLogin(?array $inputs): array
    {
        if (isset($inputs['shop_user_name']) && isset($inputs['shop_user_password'])) {
            $this->setCode(200);  // 参数正确

            $dao = $this->setter(Loader::dao('ShopUser'));  // 依赖注入
            $shopUserInfo = $dao->getInfo_byName($inputs['shop_user_name']);
            $password = passwd_hash($inputs['shop_user_password']);

            if (!$shopUserInfo || $password !== $shopUserInfo['shop_user_password']) {  // 防sql注入
                $this->setStatusCode(112006 /*通用数据不匹配*/);
                $this->setMessage('用户名或密码错误');
            } else {
                // 账号状态为启用、并且未被删除的用户才可以登录
                if ($dao->const['shop_user_limit']['enabled'] == $shopUserInfo['shop_user_limit']
                    && $dao->const['shop_user_delete_status']['enabled'] == $shopUserInfo['shop_user_delete_status']
                ) {
                    \think\Session::set('shop_user.info', $shopUserInfo);
                    unset($shopUserInfo['shop_user_password']);

                    $this->setStatusCode(0);
                    $this->setParameter('login_info', $shopUserInfo);

                    // 获取商家所有的店铺
                    $this->getShops($shopUserInfo['shop_user_customerId']);  // TODO 后期优化 消息队列
                } else {
                    $this->setStatusCode(112002 /*通用普通异常*/);
                    $this->setMessage('账号异常');
                }
            }
        }

        return $this->returnRes();
    }
}
