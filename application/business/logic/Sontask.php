<?php
// +----------------------------------------------------------------------+
// | 逻辑模型 -- 后台主页面渲染
// +----------------------------------------------------------------------+
namespace app\business\logic;

use app\business\classes\BaseLogic;
use libraries\helper\ExcelHelper;
use libraries\helper\MoneyHelper;
use think\Exception;
use think\Loader;
use think\Session;

class Sontask extends BaseLogic
{
    /**
     * 单个金额阶梯
     *
     * @param $customerOrder 客单金额
     * @return int
     */
    private function _moneyLadder($customerOrder)
    {
        if ($customerOrder < 100) {
            $customerFare = 13;
        } elseif (100 <= $customerOrder && $customerOrder < 300) {
            $customerFare = 15;
        } elseif (300 <= $customerOrder && $customerOrder < 500) {
            $customerFare = 18;
        } elseif (500 <= $customerOrder && $customerOrder < 800) {
            $customerFare = 20;
        } elseif (800 <= $customerOrder && $customerOrder < 1000) {
            $customerFare = 25;
        }

        return $customerOrder + $customerFare;;
    }

    /**
     * 对商家自行导入的任务进行md5散列加密
     *
     * @param array $lsArray Excel导入的任务列表
     * @return array         hash散列值数组
     */
    private function _importTasks_hash(?array $lsArray): array
    {
        $hashList = array();

        if ($lsArray) {
            array_walk($lsArray,
                function (&$row) use (&$hashList) {
                    $values = array_values($row);
                    sort($values, SORT_REGULAR);   // 执行字典排序
                    array_push($hashList, md5(implode('@', $values))); // 数据入栈
                }
            );
        }

        return $hashList;
    }

    /**
     * 第一次导入的Excel任务数据和第二次提交的Excel任务数据进行差异匹配
     *
     * @param array $lsArray 第二次提交的 Excel 任务列表
     * @return void
     * @throws Exception
     */
    private function _importTasks_mate(?array $lsArray): void
    {
        array_walk($lsArray, function (&$value) { unset($value['LAY_TABLE_INDEX']); }); // 移除前端layui框架的循环索引

        $hashArr = $this->_importTasks_hash($lsArray);  // 对二次提交的任务列表进行 md5 hash 散列加密
        $firstHashArr = unserialize(Session::get('importTask_hash')); // 获取第一次提交的任务列表的 hash 散列值

        array_walk($hashArr, function (&$hashVal) use (&$firstHashArr) {
            if (!in_array($hashVal, $firstHashArr)) {
                $this->setStatusCode(112006 /*数据不匹配*/);
                throw new Exception('提交的任务数据与Excel导入数据不一致');
            }
        });
    }

    /**
     * 验证商家用户账户可用余额
     *
     * @param array $params 可选多参数
     * @return void
     * @throws Exception
     */
    private function _checkAvailBalance(?array $params): void
    {
        $userId = get_shop_user_id();
        $balance = Loader::dao('ShopUser')->getAvailBalance($userId);   // 获取商家用户账户可用余额
        $totalCost = array_sum(array_column($params, 'customerOrder')); // 计划任务所需的整体预算

        if (MoneyHelper::toPenny($totalCost) > $balance) {
            $this->setStatusCode(112002 /*逻辑的异常*/);
            throw new Exception('您的账户可用余额不足, 请先充值.');
        }
    }

    /**
     * 生成任务批次号
     *
     * @param string $confuse 混淆值（任意值）
     * @return string
     */
    private function _createBachNum(string $confuse = '__hash_mtrand'): string
    {
        $uuid = create_uuid();
        return md5(sha1($uuid . $confuse . microtime()));
    }

    /**
     * 添加任务计划
     *
     * @param array $inputs
     * @return int  新增数据的ID
     * @throws Exception
     */
    private function _addTaskplan(?array $inputs): int
    {
        $info = get_shop_user();

        $lastInsertId = Loader::dao('Taskplan')->insertGetId([
            'plan_shop_id'       => $inputs['plan_shop_id'],
            'plan_shop_name'     => $inputs['plan_shop_name'],
            'plan_customer_id'   => $info['shop_user_customerId'],
            'plan_customer_name' => $info['shop_user_customer'],
            'plan_date'          => $inputs['data'][0]['taskDate'],
            'shop_user_id'       => $info['shop_user_id'],
            'shop_user_name'     => $info['shop_user_name'],
        ]);

        return $lastInsertId;
    }

    /**
     * 合并相同任务
     *
     * @description 把原先由一条拆分为多条的的任务再合并为一条
     * @param array $taskList 表单提交的任务列表
     * @return array           归类后的任务列表
     */
    private function _taskMerge(array $taskList): array
    {
        $mergeList = array();

        array_walk($taskList, function (&$task) use (&$mergeList) {
            # 每条任务只能有一个关键词. 同个商品多个关键字时必须设置为不同任务, 所以关键词不一致则视为不同任务
            $identifier = $task['badylinkid'] . $task['taskKeyWord'] . $task['customerOrder'];
            $_map = &$mergeList[$identifier];

            // 相同的任务由多条变成一条
            isset($_map) ? ($_map['tasknum'] + $task['tasknum']) : $_map = $task;
        });

        return array_values($mergeList);
    }

    /**
     * 计算佣金
     *
     * @param $customerOrder 客单金额
     * @param $taskSetNum
     */
    private function _calCommission($customerOrder, $taskSetNum)
    {
        $ladder = $this->_moneyLadder($customerOrder);
        $taskSetNum = $taskSetNum * 0.5;

        return MoneyHelper::toDollar($ladder + $taskSetNum);
    }

    /**
     * 添加任务
     *
     * @param array $inputs
     * @param $planId  附加参数
     */
    private function _addTask(?array $inputs, $planId): void
    {
        $info = get_shop_user();
        $lsArray = $this->_taskMerge($inputs['data']);
        $taskList = array();

        array_walk($lsArray,
            function (&$row, $i, $planId) use (&$info, &$inputs, &$taskList) {
                $taskList[$i] = array_merge($row, [
                    'shop_user_id'   => $info['shop_user_id'],
                    'customer_id'    => $info['shop_user_customerId'],
                    'customer_name'  => $info['shop_user_customer'],
                    'task_shop_id'   => $inputs['plan_shop_id'],
                    'task_set_num'   => intval($inputs['task_set_num']),
                    'taskStatus'     => 1,
                    'task_distinct'  => 2,
                    'taskfinishTime' => $row['taskDate'],
                    'babyLink'       => $row['taskbadyLink'],
                    'taskData'       => $row['taskKeyWord'],
                    'sbtaskNum'      => intval($row['tasknum']),
                    'task_bachnum'   => $planId,
                    'create_person'  => $info['shop_user_name'],
                    'commission'     => $this->_calCommission($row['customerOrder'], $inputs['task_set_num']),
                ]);
            }
            , $planId);

        Loader::dao('Task')->allowField(true)->saveAll($taskList);
    }

    /**
     * 获取任务管理列表
     *
     * @param array $inputs 表单输入数据
     * @return array
     * date：2019-06-12 18:11:22  by  eison
     */
    public function getList(?array $inputs): array
    {
        $this->setCode(200);
        $this->setStatusCode(112010 /*初始默认无数据*/);

        $inputs['shop_user_id'] = get_shop_user_id();
        $inputs['sontask_finish_time'] = 7; // 搜索完成的任务

        $conditions = $this->rmEmpty($inputs);
        list($list, $count) = Loader::dao('SontaskFinish')->getList_byCond($conditions);

        if ($list) {
            array_walk($list, function (&$arr) { $arr['sontask_status'] = '完成'; });
            $this->setStatusCode(0);
        }

        $this->setRetData($list);
        $this->setParameter('count', $count);

        return $this->returnRes();
    }

    /**
     * 导入任务
     *
     * @param object $binary 二进制文件对象
     * @return array
     * date：2019-06-18 16:18:41  by eison
     */
    public function importTask(?object $binary): array
    {
        $lsArray = array();
        $file = $binary->validate(['size' => 92215678, 'ext' => 'xlsx,xls'])->move(UPLOAD_PATH);

        if ($file) {
            ExcelHelper::fopen(UPLOAD_PATH . $file->getSaveName());     // 打开一个Excel文件
            $allRows = ExcelHelper::getAllRows('A,B,C,D,E,F,G,H,I', 0); // 获取Excel中指定sheet视图中所有行
            $columns = ['taskDate', 'shopName', 'taskbadyLink', 'mainFigureLink', 'customerOrder', 'taskType', 'taskKeyWord', 'timeSlot', 'remarks'];

            array_walk($allRows,
                function (&$row, $number, &$columns) use (&$lsArray) {
                    $newRow = array_combine($columns, $row);

                    $newRow['taskDate'] = ExcelHelper::specifTaskDate(trim($newRow['taskDate']));         // 标准化任务时间
                    $newRow['taskbadyLink'] = ExcelHelper::specifBadyLink(trim($newRow['taskbadyLink'])); // 标准化商品连接
                    $newRow['badylinkid'] = ExcelHelper::getBadyLinkId(trim($newRow['taskbadyLink']));    // 获取商品连接中的的商品id

                    ExcelHelper::validate($newRow, [$number]);  // 验证每一行的数据格式，确保每行数据的合法性
                    $lsArray = array_merge($lsArray, ExcelHelper::breakTasks($newRow));  // 所有任务条数列表
                }
                , $columns);

            $hashArr = $this->_importTasks_hash($lsArray); // 对商家自行导入的每条任务进行md5散列加密，保证下次提交数据的可靠性
            Session::set('importTask_hash', serialize($hashArr)); // TODO 后期优化：缓存 + 消息队列

            $this->setParameter('total_task_arr_num', $lsArray);
            $this->setCode(200);
            $this->setStatusCode(0);
        } else {
            $this->setStatusCode(112007 /*通用数据格式错误*/);
            $this->setMessage($binary->getError());
        }

        return $this->returnRes();
    }

    /**
     * 支付计划费用
     *
     * @param array|null $input
     * @return array
     * date：2019-06-19 14:09:39  by eison
     */
    public function checkout(?array $inputs): array
    {
        if (Session::get('importTask_hash') && isset($inputs['data'])) {
            $this->setCode(200);

            $lsArray = json_decode($inputs['data'], true);
            $inputs['data'] = &$lsArray;

            try {
                $this->_importTasks_mate($lsArray);   // 确定数据的合法性
                $this->_checkAvailBalance($lsArray);  // 验证商家用户账户可用余额

                // \think\Db::startTrans();
                $bachNum = $this->_createBachNum();
                $planId = $this->_addTaskplan($inputs); // 添加计划任务
                $this->_AddTask($inputs, $planId);      // 添加主任务

                p($inputs);

                // \think\Db::commit();
            } catch (Exception $e) {
                $this->setMessage($e->getMessage());
                \think\Db::rollback();
            }
        }

        return $this->returnRes();
    }
}
