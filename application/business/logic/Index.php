<?php
// +----------------------------------------------------------------------+
// | 逻辑模型 -- 后台主页面渲染
// +----------------------------------------------------------------------+
namespace app\business\logic;

use app\business\classes\BaseLogic;
use think\Loader;

class Index extends BaseLogic
{
    /**
     * 后台主页面渲染
     *
     * @param array $inputs 表单输入数据
     * @return array
     * date：2019-06-12 16:08:39  by  eison
     */
    public function index(?array $inputs): array
    {
        if (isset($inputs['business_roseId'])) {
            $this->setCode(200);

            $menus = Loader::dao('BusinessMenu')->getMenus();
            $roseId = (string)$inputs['business_roseId'];

            if ($menus) {
                foreach ($menus as $arr) {
                    if (strstr($arr['business_menu_authority'], $roseId)) {
                        $list[$arr['parentid']][] = $arr;
                    }
                }
            }

            $this->setStatusCode(0);
            $this->setRetData($list);
        }

        return $this->returnRes();
    }
}
