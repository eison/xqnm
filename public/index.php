<?php
// [ 应用入口文件 ]

// 检测PHP环境
if (version_compare(PHP_VERSION, '7.2', '<')) {
    die('require PHP > 7.2 !');
}

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';